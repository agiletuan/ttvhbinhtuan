<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ttvh');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'vertrigo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         's]s(ieL,B7fy/H{SZt.VlVb_HR}&^{>(#gnZnplWuVgU<a(!>Wkc1S8F2JAkUxi-');
define('SECURE_AUTH_KEY',  '+N!sUmmUm_2v>h qAggb7g !e_yC3waX FJvc{;;brqxec?;ckQh?MP(>AwOQBEw');
define('LOGGED_IN_KEY',    'C5oE_^a&i(hh-wbA%I&9IN[J!HLkX|z#-E%-lB(J0`FY$PLqw/Cqj;35nBA&}Cx{');
define('NONCE_KEY',        'KI;N~R6`jgDP_BtyOL!dFaCWAzXc%ySu;$rRQ`chs-Xn6Zu]_tio7ti$_UWG7HWD');
define('AUTH_SALT',        '(o:|)2Jbe/ydp@&H`9NfR)9?ks1{;L-eU#aEDoA9 =v{;+-+o>hho<C4vqo4y]J&');
define('SECURE_AUTH_SALT', '*a&w0!-J;47H`vwzAs3:^NturI&WaGa$)!hJ6<5txS[qk^SNxm$DxuDxR<FI!PXo');
define('LOGGED_IN_SALT',   'RoYo1g?NsTtyLObiJn?|j#8SJ}mAS(2ST2AX4yt/XSqtv&$n3uy@tg*B:`2?<2~a');
define('NONCE_SALT',       '^FIu9&j=oZKK-<IC>I+lgt42{Ia?EE9+z$F0w]]ny7QrS%L&ky6Rvi[?(#$/ZcB ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
